function uninstall_"$package" -V path
  wget -qO $path/dependency.fish \
  https://gitlab.com/lusiadas/dependency/raw/master/dependency.fish
  source $path/dependency.fish
  and dependency -rP feedback -P contains_opts sed grep pastebinit \
  (type -qf termux-info; or echo xclip)
end
uninstall_"$package"
functions -e dependency update_history uninstall_"$package"
