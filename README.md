# publish
> A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v2.7.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

## Description

A wrapper function for pastebinit, allowing it to publish multiple lines at once while also choosing which lines to send. By not specifying a target file, or passing content through a pipe, the contents of the clipboard are uploaded instead. This function is fully compatible with [termux](termux.com) as well.


## Example usage
[![asciicast](https://asciinema.org/a/chd0kvdSdtw33CWMzn5vsM3tS.png)](https://asciinema.org/a/chd0kvdSdtw33CWMzn5vsM3tS)

## Options
```
-l/--lines [0-7,8,9]
For each file, set the line range to be published

-a/--author [author]
Set author name. Default is $USER

-b/--pastebin [url]]
Set pastebin url. Default is distro specific with fallback to pastebin.

-e/--echo
Print content to stdout too

-f/--format [format]
Choose a highlighting format (check pastebin's website for complete list, example: python). Default is "text".

-h/--help
Display these instructions

-i/--filename
Use filename for input

-L/--list
List supported pastebins

-j/--jabberid [id]
Set Jabber ID

-m/--permatag [permatag]
Set permatag

-t/--title [title]
For each file, set a title

-P/--private [0/1]
Make paste(s) private

-u/--username [username]
Set a username

-p/--password [password]
Set a password

-v/--version
Print pastebinit version
```

## Dependencies

Publish uses `pastebinit` to send files and the `feedback` plugin to print results. On Linux, publish uses `xclip` to access clipboard content. While on Termux, publish uses `termux-clipboard-get` and `termux-clipboard-set` for this same purpose. Both of them are available by installing the meta-package `termux-api` (see installation instructions for it at https://wiki.termux.com/wiki/Termux:API)

If not for `termux-api`, If you haven't already installed any of these dependences, you'll be prompted to do so upon running `publish`.

## Install

Either with omf
```fish
omf install publish
```
___

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
